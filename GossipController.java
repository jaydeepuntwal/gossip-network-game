/**
 * GossipController.java
 * 
 * Controller of game Gossip
 *
 * @author Jaydeep Untwal
 * @author Shubham Saxena
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class GossipController extends UnicastRemoteObject implements
		GossipControllerInterface {

	String who;

	public boolean goAhead = false;
	public boolean updated = false;
	String str = "";

	// private Socket sock;

	/**
	 * Create the application.
	 */
	public GossipController(String pl) throws RemoteException {
		// this.sock = sock;
		this.who = pl;
		GossipView gv = new GossipView();
		gv.initialize(actionListener, pl);
	}

	/**
	 * Print the board
	 * 
	 * @param a
	 */
	private void print(int a[][]) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				System.out.print(" " + a[i][j]);
			}
			System.out.println("\n");
		}
	}

	/*
	 * Check if the game has come to an end
	 */
	private void checkOver() {

		GossipModel gm = new GossipModel();
		GossipView gv = new GossipView();

		int count_total = 0;

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {

				int board[][] = gm.getBoard();

				if (board[i][j] > 0) {
					count_total++;
				}
			}
		}

		if (count_total == 64) {

			if (gm.getP1Score() > gm.getP2Score()) {
				gv.displayMessage("Game Over !");
			//	System.exit(0);
			} else if (gm.getP1Score() < gm.getP2Score()) {
				gv.displayMessage("Game Over ! ");
				//System.exit(0);
			} else {
				gv.displayMessage("Game Over ! It is a draw !");
			//	System.exit(0);
			}
		}
	}

	/**
	 * Transform blocks on each click
	 */
	private void transform() {

		GossipView gv = new GossipView();
		GossipModel gm = new GossipModel();
		int board[][] = gm.getBoard();

		for (int i = 0; i < 8; i++) {

			if (gm.getHline().contains(new Integer(i)) == false) {
				int countR = 0;
				int countB = 0;

				for (int j = 0; j < 8; j++) {

					if (board[i][j] == 1) {
						countR++;
					} else if (board[i][j] == 2) {
						countB++;
					}
				}

				if (countR >= 4) {
					for (int j = 0; j < 8; j++) {
						board[i][j] = 1;
						gv.setButtonBackground(i, j, gv.getP1C());
						gv.getButton(i, j).removeActionListener(actionListener);

						if (gm.getHline().add(new Integer(i))) {
							gm.setP1Score(gm.getP1Score() + 1);
							gv.setScoreP1("Player 1: " + gm.getP1Score() + " ");
						}
					}
				} else if (countB >= 4) {
					for (int j = 0; j < 8; j++) {
						board[i][j] = 2;
						gv.setButtonBackground(i, j, gv.getP2C());
						gv.getButton(i, j).removeActionListener(actionListener);

						if (gm.getHline().add(new Integer(i))) {
							gm.setP2Score(gm.getP2Score() + 1);
							gv.setScoreP2("Player 2: " + gm.getP2Score() + " ");
						}
					}
				}

			}

		}

		for (int i = 0; i < 8; i++) {

			if (gm.getVline().contains(new Integer(i)) == false) {
				int countR = 0;
				int countB = 0;

				for (int j = 0; j < 8; j++) {
					if (board[j][i] == 1) {
						countR++;
					} else if (board[j][i] == 2) {
						countB++;
					}
				}

				if (countR >= 3) {
					for (int j = 0; j < 8; j++) {
						board[j][i] = 1;
						gv.setButtonBackground(j, i, gv.getP1C());
						gv.getButton(j, i).removeActionListener(actionListener);

						if (gm.getVline().add(new Integer(i))) {
							gm.setP1Score(gm.getP1Score() + 1);
							gv.setScoreP1("Player 1: " + gm.getP1Score() + " ");
							gv.setComments("Player 2, your image is on the stake!!!");

						}
					}
				} else if (countB >= 3) {
					for (int j = 0; j < 8; j++) {
						board[j][i] = 2;
						gv.setButtonBackground(j, i, gv.getP2C());
						gv.getButton(j, i).removeActionListener(actionListener);

						if (gm.getVline().add(new Integer(i))) {
							gm.setP2Score(gm.getP2Score() + 1);
							gv.setScoreP2("Player 2: " + gm.getP2Score() + " ");
							gv.setComments("Player 1, your image is on the stake!!!");
						}
					}
				}

			}

		}

		// print(board);

		checkOver();

	}

	/**
	 * Mouse Click action listener for button
	 */
	ActionListener actionListener = new ActionListener() {
		public void actionPerformed(ActionEvent actionEvent) {

			GossipModel gm = new GossipModel();
			GossipView gv = new GossipView();

			// Something in this if loop should stop them from clicking

			//System.out.println("Player chance: " + gm.getPlayer());

			if (gm.getPlayer() == 1) {
				str = gv.changeButton(actionEvent, gm, actionListener);
				transform();
				// System.out.println(str);
				try {

					// Part where communication happens

					goAhead = true;

					/*
					 * PrintWriter out = new PrintWriter(sock.getOutputStream(),
					 * true); out.println(str);
					 */
				} catch (Exception e) {
				}
			}
		}

	};

	public void update(int i, int j, int player, int k) throws RemoteException {
		System.out.println("updating");
		GossipView gv = new GossipView();
		GossipModel gm = new GossipModel();
		int board[][] = gm.getBoard();
		/*
		 * for (int row = 0; row < 8; row++) for (int col = 0; col < 8; col++)
		 * gv.getButton(row,col).addActionListener(actionListener);
		 */

		if (player == 2) {
			board[i][j] = 2;
			gv.getButton(i, j).setBackground(gv.getP2C());
			gm.setPlayer(1);
			gv.getLblNewLabel().setText(
					"Player 1, It is your turn to spread the gossip!");

		} else if (player == 1) {
			board[i][j] = 1;
			gv.getButton(i, j).setBackground(gv.getP1C());
			gm.setPlayer(2);
			gv.getLblNewLabel().setText(
					"Player 2, It is your turn to spread the gossip!");
		}

		gv.getButton(i, j).removeActionListener(actionListener);
		transform();

		updated = true;

	}
}

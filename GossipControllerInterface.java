import java.rmi.*;

public interface GossipControllerInterface extends Remote {
	public void update(int i, int j, int player, int k) throws RemoteException;
}

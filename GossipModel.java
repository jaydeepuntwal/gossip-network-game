/**
 * GossipModel.java
 * 
 * Model of game Gossip
 *
 * @author Jaydeep Untwal
 * @author Shubham Saxena
 */
import java.util.HashSet;
import java.util.Set;

public class GossipModel {

	protected static int p1Score = 0; // Player 1 score
	protected static int p2Score = 0; // Player 2 score
	protected static int player = 1; // Current Player
	protected static int board[][] = new int[8][8]; // Play board

	// Captured horizontal lines
	protected static Set<Integer> Hline = new HashSet<Integer>();
	// Captured Vertical lines
	protected static Set<Integer> Vline = new HashSet<Integer>();

	public int getP1Score() {
		return p1Score;
	}

	public void setP1Score(int p1Score) {
		this.p1Score = p1Score;
	}

	public int getP2Score() {
		return p2Score;
	}

	public void setP2Score(int p2Score) {
		this.p2Score = p2Score;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int[][] getBoard() {
		return board;
	}

	public void setBoard(int[][] board) {
		this.board = board;
	}

	public Set<Integer> getHline() {
		return Hline;
	}

	public void setHline(Set<Integer> hline) {
		Hline = hline;
	}

	public Set<Integer> getVline() {
		return Vline;
	}

	public void setVline(Set<Integer> vline) {
		Vline = vline;
	}

}

/**
 * Gossip.java
 * 
 * Gossip is a challenging round-based game played by two players using just
 * mouse clicks.
 * 
 * Objective: Spread the gossip to as many blocks as you can, If you spread
 * gossip to 4 horizontal blocks or 3 vertical blocks, the line is yours. Each
 * line adds 1 to the score. Player with more number of lines wins at the end
 * wins.
 * 
 * Rules: Each player can click on only one block in one chance.
 * 
 * Have fun !
 * 
 * @author Jaydeep Untwal
 * @author Shubham Saxena
 * @contributor Adwait Kirtikar
 */

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class GossipServer {

	public static void main(String args[]) throws RemoteException,
			InterruptedException, MalformedURLException, NotBoundException {

		String host = (args.length > 0) ? args[0] : "localhost";

		GossipController gc = new GossipController("Player 1");
		Naming.rebind("rmi://" + host + "/P1", gc);

		System.out.println("registered");
		while (true) {

			while (gc.goAhead == false) {
				Thread.sleep(500);
				System.out.println("Play");
			}
			gc.goAhead = false;

			GossipControllerInterface anotherPlayer = (GossipControllerInterface) Naming
					.lookup("rmi://" + host + "/P2");
			System.out.println("Got the reference for updating");
			String indexString = gc.str;
			String buttonIndex[] = indexString.split(",");
			int i = new Integer(buttonIndex[0]).intValue();
			int j = new Integer(buttonIndex[1]).intValue();
			int k = new Integer(buttonIndex[2]).intValue();
			anotherPlayer.update(i, j, 2, k);

			while (gc.updated == false) {
				Thread.sleep(500);
			}

			gc.updated = false;
		}
	}

}

/**
 * GossipView.java
 * 
 * View of game Gossip
 *
 * @author Jaydeep Untwal
 * @author Shubham Saxena
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GossipView {

	protected static JFrame frame = new JFrame("Gossip");
	protected static Color p1C = new Color(255, 0, 0);
	protected static Color p2C = new Color(0, 0, 0);

	protected static JButton button[][] = new JButton[8][8];

	// Heading
	protected static JLabel lblNewLabel = new JLabel(
			"Welcome to Gossip, Player 1, it is your turn to spread the gossip!");

	// Player 1 Score
	protected static JLabel ScoreP1 = new JLabel("Player 1:    ");
	// Player 2 Score
	protected static JLabel ScoreP2 = new JLabel("Player 2:    ");
	// Game comments
	protected static JLabel Comments = new JLabel(" ");

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize(ActionListener actionListener, String pl) {

		lblNewLabel
				.setText(pl
						+ ", Welcome to Gossip, Player 1, it is your turn to spread the gossip!");
		frame.setVisible(true);

		// Main Panel
		frame.setBounds(100, 100, 450, 300);
		frame.setMinimumSize(new Dimension(640, 400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();

		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(8, 8, 0, 0));

		// Add Buttons
		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < 8; j++) {
				JButton butt = new JButton();

				butt.setBorderPainted(true);
				butt.addActionListener(actionListener);

				button[i][j] = butt;
				panel.add(butt);
			}

		}

		// Add Heading
		lblNewLabel.setFont(new Font("DejaVu Serif", Font.BOLD, 17));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setVerticalAlignment(SwingConstants.CENTER);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBackground(Color.WHITE);

		JPanel panelBottom = new JPanel();

		panelBottom.setBackground(new Color(0, 28, 56));
		frame.getContentPane().add(panelBottom, BorderLayout.NORTH);

		panelBottom.add(lblNewLabel);

		// Add player 1 score
		ScoreP1.setFont(new Font("DejaVu Serif", Font.BOLD, 20));
		ScoreP1.setForeground(Color.WHITE);
		ScoreP1.setVerticalAlignment(SwingConstants.CENTER);
		ScoreP1.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreP1.setBackground(Color.WHITE);

		JPanel panelBottomP1 = new JPanel();

		panelBottomP1.setBackground(new Color(0, 28, 56));
		frame.getContentPane().add(panelBottomP1, BorderLayout.WEST);

		panelBottomP1.add(ScoreP1);

		// Add Player 2 score
		ScoreP2.setFont(new Font("DejaVu Serif", Font.BOLD, 20));
		ScoreP2.setForeground(Color.WHITE);
		ScoreP2.setVerticalAlignment(SwingConstants.CENTER);
		ScoreP2.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreP2.setBackground(Color.WHITE);

		JPanel panelBottomP2 = new JPanel();

		panelBottomP2.setBackground(new Color(0, 28, 56));
		frame.getContentPane().add(panelBottomP2, BorderLayout.EAST);

		panelBottomP2.add(ScoreP2);

		// Add comments
		Comments.setFont(new Font("DejaVu Serif", Font.BOLD, 20));
		Comments.setForeground(Color.WHITE);
		Comments.setVerticalAlignment(SwingConstants.CENTER);
		Comments.setHorizontalAlignment(SwingConstants.CENTER);
		Comments.setBackground(Color.WHITE);

		JPanel panelBottomComment = new JPanel();

		panelBottomComment.setBackground(new Color(0, 28, 56));
		frame.getContentPane().add(panelBottomComment, BorderLayout.SOUTH);

		panelBottomComment.add(Comments);

	}

	public JButton[][] getButton() {
		return button;
	}

	public JButton getButton(int i, int j) {
		return button[i][j];
	}

	public void setButton(JButton[][] button) {
		this.button = button;
	}

	public void setButtonBackground(int i, int j, Color c) {
		button[i][j].setBackground(c);
	}

	public String changeButton(ActionEvent actionEvent, GossipModel gm,
			ActionListener actionListener) {
		JButton butTemp = (JButton) actionEvent.getSource();

		int board[][] = gm.getBoard();
		String str = "";
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (button[i][j].equals(butTemp)) {
					board[i][j] = gm.getPlayer();
					str = i + "," + j + "," + gm.getPlayer();
					break;
				}
			}
		}

		if (gm.getPlayer() == 1) {
			butTemp.setBackground(p1C);
			gm.setPlayer(2);
			butTemp.removeActionListener(actionListener);
			lblNewLabel
					.setText("Player 2, It is your turn to spread the gossip!");
		} else {
			butTemp.setBackground(p2C);
			gm.setPlayer(1);
			butTemp.removeActionListener(actionListener);
			lblNewLabel
					.setText("Player 1, It is your turn to spread the gossip!");
		}

		/*
		 * for (int i = 0; i < 8; i++) for (int j = 0; j < 8; j++)
		 * button[i][j].removeActionListener(actionListener);
		 */

		return str;
	}

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}

	public void setLblNewLabel(JLabel lblNewLabel) {
		this.lblNewLabel = lblNewLabel;
	}

	public JLabel getScoreP1() {
		return ScoreP1;
	}

	public void setScoreP1(String message) {
		ScoreP1.setText(message);
	}

	public JLabel getScoreP2() {
		return ScoreP2;
	}

	public void setScoreP2(String message) {
		ScoreP2.setText(message);
	}

	public JLabel getComments() {
		return Comments;
	}

	public void setComments(String message) {
		Comments.setText(message);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public Color getP1C() {
		return p1C;
	}

	public void setP1C(Color p1c) {
		p1C = p1c;
	}

	public Color getP2C() {
		return p2C;
	}

	public void setP2C(Color p2c) {
		p2C = p2c;
	}

	public void displayMessage(String message) {
		JOptionPane.showMessageDialog(frame, message);
	}

	public boolean checkButtonAction(int i, int j, ActionEvent actionEvent) {
		if (button[i][j].equals((JButton) actionEvent.getSource())) {
			return true;
		}

		return false;
	}

}
